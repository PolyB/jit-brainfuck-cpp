#include <iostream>
#include <sstream>
#include <fstream>


#include "Brainfuck.hh"

void out(char c) noexcept
{
  printf("%c", c);
}

static char in() noexcept
{
  char i;
  std::cin >> i;
  return i;
}

int main()
{
  Brainfuck::run(std::cin, out, in);
  std::cout << std::endl;
}
