#pragma once

#include <sys/mman.h>
#include <unistd.h>
#include <iostream>

#include "asm.hh"

static const size_t pagesize = sysconf(_SC_PAGESIZE);

using outfp =  void(*)(char) noexcept;
using infp  = char(*)() noexcept;

class Brainfuck
{
  public:
    static void run(std::istream& input, outfp out, infp in);
};
