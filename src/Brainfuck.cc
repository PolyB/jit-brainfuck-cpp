#include "Brainfuck.hh"

#include <stack>
#include <vector>
#include <new> // bad_alloc
#include <iostream>
#include <cstring>

using pagefp = void(*)(outfp, infp, char*mem) noexcept;

void Brainfuck::run(std::istream& input, outfp out, infp in)
{
  void* const page = mmap(nullptr, pagesize * 4, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  void *const mem = mmap(nullptr, pagesize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (page == MAP_FAILED)
    throw std::bad_alloc();
  std::stack<char*, std::vector<char*>> stack ={};

  char* page_instr = reinterpret_cast<char*>(page);

   {
     std::memcpy(page_instr, PRELUDE, sizeof (PRELUDE)-1);
     page_instr += sizeof (PRELUDE) -1;
   }
  while (!input.eof())
  {
    switch (input.get())
    {
      case '>':
      {
        std::memcpy(page_instr, RIGHT, sizeof (RIGHT)-1);
        page_instr += sizeof (RIGHT) - 1;
        break;
      }
      case '<':
      {
        std::memcpy(page_instr, LEFT, sizeof (LEFT)-1);
        page_instr += sizeof (LEFT) -1;
        break;
      }
      case '.':
      {
        std::memcpy(page_instr, PRINT, sizeof (PRINT)-1);
        page_instr += sizeof (PRINT) -1;
        break;
      }
      case ',':
      {
        std::memcpy(page_instr, GET, sizeof (GET)-1);
        page_instr += sizeof (GET) -1;
        break;
      }
      case '+':
      {
        std::memcpy(page_instr, INC, sizeof (INC)-1);
        page_instr += sizeof (INC) -1;
        break;
      }
      case '-':
      {
        std::memcpy(page_instr, DEC, sizeof (DEC)-1);
        page_instr += sizeof (DEC) -1;
        break;
      }
      case '[':
      {
        std::memcpy(page_instr, BEG, sizeof (BEG)-1);
        stack.push(page_instr);
        page_instr += sizeof (BEG) -1;
        break;
      }
      case ']':
      {
        if (stack.empty())
          throw std::runtime_error("not-open ']'");

        std::memcpy(page_instr, END, sizeof (END)-1);
        *reinterpret_cast<std::int32_t*>(page_instr + sizeof (END) - 1 - 4) = (stack.top() - (page_instr + (sizeof (END) - 1)));
        *reinterpret_cast<std::int32_t*>(stack.top() + sizeof (BEG) - 1 - 4) =  (page_instr - (stack.top() + (sizeof (BEG) - 1)));
        page_instr += sizeof (END) -1;
        stack.pop();
        break;

      }
    }
  }
  {
    std::memcpy(page_instr, EPILOG, sizeof (EPILOG)-1);
    page_instr += sizeof (EPILOG) -1;
  }
  if (!stack.empty())
    throw std::runtime_error("non-closed '['");
  reinterpret_cast<pagefp>(page)(out, in, reinterpret_cast<char*>(mem));
}
